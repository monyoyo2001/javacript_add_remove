//lấy id của thẻ form
var form = document.getElementById('addForm');
//lấy id của thẻ ul
var itemList = document.getElementById('items');

//lắng nghe sự kiện 
form.addEventListener('submit', addItem);

itemList.addEventListener('click', removeItem);

//thêm item
function addItem(e){
    e.preventDefault();
    //lấy giá trị nhập vô thẻ input
    var newItem = document.getElementById('item').value;
    
    //tạo mới một thẻ li
    var li = document.createElement('li');

    //thêm class cho thẻ li
    li.className = 'list-group-item';
    
    //thêm giá trị thẻ input vô thẻ li 
    li.appendChild(document.createTextNode(newItem));

    // tạo mới một nút xóa
    var deleteBtn = document.createElement('button');

    //thêm class cho thẻ button
    deleteBtn.className ='btn btn-secondary btn-sm float-right delete';
    
    //thêm text vào thẻ button
    deleteBtn.appendChild(document.createTextNode('X'));

    //thêm thẻ button vào li
    li.appendChild(deleteBtn);

    //thêm thẻ li vào thẻ ul
    itemList.appendChild(li);
}

//xóa item
function removeItem(e){
    if(e.target.classList.contains('delete')){
        if(confirm('Bạn có muốn xóa không?')){
            var li = e.target.parentElement;
            itemList.removeChild(li);
        }
    }
}
